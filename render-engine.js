var RenderEngine = RenderEngine || {};
var gl;

RenderEngine = function(canvasId) {
	var canvas = document.getElementById(canvasId);

	try {
		// Try to grab the standard context. If it fails, fallback to experimental.
		gl = canvas.getContext("webgl") || canvas.getContext("experimental-webgl");
		gl.enable(gl.DEPTH_TEST);
		gl.depthFunc(gl.LEQUAL);
	}
	catch(e) {
		console.log(e.stack);
		return;
	}

	this.layers = [];
};

RenderEngine.prototype.addLayer = function(layer) {
	this.layers.push(layer);
};

RenderEngine.prototype.render = function() {
	var layer;
	var depth = 0.0;
	for (var i = 0; i < this.layers.length; i++) {
		layer = this.layers[i];
		layer.setupRender();
		layer.render(depth);
		depth -= 1.0;
	}
	//gl.clear(gl.COLOR_BUFFER_BIT|gl.DEPTH_BUFFER_BIT);
};