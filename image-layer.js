var ImageLayer = ImageLayer || {};

ImageLayer.prototype = Object.create(RenderLayer.prototype);
ImageLayer = function(image) {
	var vertexShader = compileShaderFromScript("image-shader-vs");
	var fragmentShader = compileShaderFromScript("image-shader-fs");

	this.program = compileProgram(vertexShader, fragmentShader);
	gl.useProgram(this.program);

	var positionLocation = gl.getAttribLocation(this.program, "a_position");
	var texCoordLocation = gl.getAttribLocation(this.program, "a_texCoord");
	this.scaleLocation = gl.getUniformLocation(this.program, "u_scaleMatrix");
	this.translateLocation = gl.getUniformLocation(this.program, "u_translateMatrix");
	this.rotatelocation = gl.getUniformLocation(this.program, "u_rotateMatrix");
	this.depthLocation = gl.getUniformLocation(this.program, "u_depth");


	this.scaleMatrix = mat4.identity(mat4.create());
	this.translateMatrix = mat4.identity(mat4.create());
	this.rotateMatrix = mat4.identity(mat4.create());

	this.texture = gl.createTexture();
	gl.bindTexture(gl.TEXTURE_2D, this.texture);
 
	// Set up texture so we can render any size image and so we are
	// working with pixels.
	gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_S, gl.CLAMP_TO_EDGE);
	gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_T, gl.CLAMP_TO_EDGE);
	gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.NEAREST);
	gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, gl.NEAREST);

	gl.texImage2D(gl.TEXTURE_2D, 0, gl.RGBA, gl.RGBA, gl.UNSIGNED_BYTE, image);


	this.buffer = gl.createBuffer();
	gl.bindBuffer(gl.ARRAY_BUFFER, this.buffer);
	gl.bufferData(
		gl.ARRAY_BUFFER, 
		new Float32Array([
			-1.0, -1.0,
			 0.0,  1.0,
			 1.0, -1.0,
			 1.0,  1.0,
			-1.0,  1.0,
			 0.0,  0.0,
			 1.0,  1.0,
			 1.0,  0.0]),
		gl.STATIC_DRAW);
	gl.enableVertexAttribArray(positionLocation);
	gl.enableVertexAttribArray(texCoordLocation);
	gl.vertexAttribPointer(positionLocation, 2, gl.FLOAT, false, 16, 0);
	gl.vertexAttribPointer(texCoordLocation, 2, gl.FLOAT, false, 16, 8);
};

ImageLayer.prototype.scale = function(x, y) {
	mat4.scale(this.scaleMatrix, this.scaleMatrix, [x, y, 1]);
}

ImageLayer.prototype.translate = function(x, y) {
	mat4.translate(this.translateMatrix, this.translateMatrix, [x, y, 0	]);
}

ImageLayer.prototype.rotate = function(angle) {
	mat4.rotateZ(this.rotateMatrix, this.rotateMatrix, angle);
}

ImageLayer.prototype.setupRender = function() {
	gl.useProgram(this.program);

	gl.bindBuffer(gl.ARRAY_BUFFER, this.buffer);
};

ImageLayer.prototype.render = function(depth) {
	gl.uniformMatrix4fv(this.scaleLocation, false, this.scaleMatrix);
	gl.uniformMatrix4fv(this.translateLocation, false, this.translateMatrix);
	gl.uniformMatrix4fv(this.rotatelocation, false, this.rotateMatrix);
	gl.uniform1f(this.depthLocation, depth);

	gl.bindTexture(gl.TEXTURE_2D, this.texture);
	gl.drawArrays(gl.TRIANGLE_STRIP, 0, 4);
};